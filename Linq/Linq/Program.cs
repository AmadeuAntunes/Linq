﻿using ClassLibrary;
using System;
using System.Collections.Generic;
using System.Linq;

namespace Linq
{
    class Program
    {
        static void Main(string[] args)
        {
            List<Aluno> Alunos = ListaDeAlunos.loadAlunos();

            //Odenar por apelido

            Alunos = Alunos.OrderBy(x => x.Apelido).ToList();

            //Odenar por apelido
            Alunos = Alunos.OrderByDescending(x => x.Apelido).ToList();


            //Odenar por apelido
            Alunos = Alunos.OrderByDescending(x => x.Apelido).
                ThenByDescending(x => x.DisciplinasFeitas).
                ToList();


            //Odenar por apelido
            Alunos = Alunos.Where(x => x.DisciplinasFeitas > 10 && x.DataNascimento.Month == 3).ToList();

            foreach (var aluno in Alunos)
            {
                Console.WriteLine($"{aluno.PrimeiroNome} {aluno.Apelido} {aluno.DataNascimento.ToShortDateString()} Disciplinas Feitas: {aluno.DisciplinasFeitas}");

            }

            int TotalDisciplinasFeitas = Alunos.Sum(x => x.DisciplinasFeitas);
            double MediaDisciplinasFeitas = Alunos.Average(x => x.DisciplinasFeitas);

            Console.WriteLine($"Total de disciplinas feitas: {TotalDisciplinasFeitas}");
            Console.WriteLine($"Media de disciplinas feitas: {MediaDisciplinasFeitas:n2}");
            Console.ReadKey();

        }
    }
}
