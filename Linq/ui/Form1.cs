﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using ClassLibrary;
namespace ui
{
    public partial class Form1 : Form
    {
        List<Aluno> Alunos = ListaDeAlunos.loadAlunos();
        public Form1()
        {
            InitializeComponent();
            InitCombo();
        }

        private void InitCombo()
        {
            ComboBoxTodosOsAlunos.DataSource = Alunos;
            ComboBoxTodosOsAlunos.DisplayMember = "NomeCompleto";

            ListBoxFiltro.DataSource = Alunos.Where(x => x.DisciplinasFeitas > 10).
                OrderBy(x => x.PrimeiroNome).
                ThenBy(x => x.Apelido).ToList();
            ListBoxFiltro.DisplayMember = "NomeCompleto";
        }

        private void ListBoxFiltro_SelectedIndexChanged(object sender, EventArgs e)
        {
            Aluno alunoSelecionado = (Aluno)ComboBoxTodosOsAlunos.SelectedItem;

            numericUpDownFeitas.Value = alunoSelecionado.DisciplinasFeitas;
        }

        private void button1_Click(object sender, EventArgs e)
        {
            Aluno alunoSelecionado = (Aluno)ComboBoxTodosOsAlunos.SelectedItem;
            alunoSelecionado.DisciplinasFeitas = Convert.ToInt32(numericUpDownFeitas.Value);
            UpdateDate();
        }


        private void UpdateDate()
        {
            ListBoxFiltro.DataSource = Alunos.Where(x => x.DisciplinasFeitas > 10).
               OrderBy(x => x.PrimeiroNome).
               ThenBy(x => x.Apelido).ToList();
        }
    }
}
